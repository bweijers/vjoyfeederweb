﻿using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace VjoyFeederWebSH
{
    public class Program
    {
        static void Main(string[] args)
        {
            var hostname = Dns.GetHostName();
            var ipAddresses = Dns.GetHostEntry(hostname).AddressList;
            var protocolPrefix = "http://";
            var port = 80;

            var baseAddress = $"{protocolPrefix}*:{port}";



            //var startOptions = new StartOptions
            //{
            //    Port = 80
            //};

            using (WebApp.Start<Startup>(baseAddress))
            {
                Console.Out.WriteLine($"Server is running");
                Console.Out.WriteLine($"Hostname: {hostname}");
                Console.Out.WriteLine($"Port: {port}");
                Console.Out.WriteLine("IP Addresses:");
                ipAddresses.All(x => { Console.Out.WriteLine(x.ToString()); return true; });
                Console.In.ReadLine();
            }
        }
    }
}
