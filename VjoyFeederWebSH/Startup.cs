﻿using Microsoft.Owin.FileSystems;
using Microsoft.Owin.StaticFiles;
using Owin;
using Owin.WebSocket.Extensions;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.IO;
using System.Reflection;
using System.Web.Http;

namespace VjoyFeederWebSH
{
    public class Startup
    {
        public static ServiceContainer Container { get; set; }

        public void ConfigureServices()
        {
            Container = new ServiceContainer();

            Container.AddService(typeof(WebStick), new WebStick());
        }

        // This cod configures the Web API. The Startup class is specified as a type parameter in the WebApp.Start method.
        public void Configuration(IAppBuilder appBuilder)
        {
            ConfigureServices();

            // Configure the Web API for self-host
            //HttpConfiguration config = new HttpConfiguration();

            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional });

            appBuilder.MapWebSocketRoute<WebStick>("/ws");

            appBuilder.UseDefaultFiles(new DefaultFilesOptions
            {
                DefaultFileNames = new List<string> { "index.html" },
                RequestPath = new Microsoft.Owin.PathString(""),
                FileSystem = new PhysicalFileSystem(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "wwwroot"))
            });

            appBuilder.UseStaticFiles(new StaticFileOptions
            {
                RequestPath = new Microsoft.Owin.PathString(""),
                FileSystem = new PhysicalFileSystem(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "wwwroot"))
            });

            //appBuilder.UseWebApi(config);
        }
    }
}
