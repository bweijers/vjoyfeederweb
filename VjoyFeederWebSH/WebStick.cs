﻿using Microsoft.Owin.Logging;
using Owin.WebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading.Tasks;
using vJoyInterfaceWrap;

namespace VjoyFeederWebSH
{
    public class WebStick : WebSocketConnection
    {
        private uint _stickId = 1;
        private vJoy _stick;
        private int _buttonCount = 0;

        public WebStick()
        {
            Console.Out.WriteLine("New instance of WebStick created");
        }

        public override void OnOpen()
        {
            Console.Out.WriteLine("Client connected");
            base.OnOpen();
        }

        public override void OnClose(WebSocketCloseStatus? closeStatus, string closeStatusDescription)
        {
            Console.Out.WriteLine($"Client disconnected - {closeStatus.GetValueOrDefault()} - {closeStatusDescription}");

            if (_stick != null)
            {
                _stick.RelinquishVJD(_stickId);
            }

            base.OnClose(closeStatus, closeStatusDescription);
        }

        public async override Task OnMessageReceived(ArraySegment<byte> message, WebSocketMessageType type)
        {
            // Handle the message from the client

            var data = System.Text.Encoding.Default.GetString(message.Array, message.Offset, message.Count);

            Console.Out.WriteLine($"Received message: {data}");
            var parts = data.Split(' ');

            if (parts.Length != 3)
            {
                //return Task.FromResult("Invalid request data");
                await SendText(Encoding.Default.GetBytes("Invalid request data"), true);
            }

            InitStick();

            switch(parts[0].ToUpper())
            {
                case "BTN":
                    var btnId = uint.Parse(parts[2]);
                    if (btnId > _buttonCount)
                    {
                        Console.Error.WriteLine($"Invalid button id {btnId}");
                    }
                    switch(parts[1].ToUpper())
                    {
                        case "PRESS":
                            Console.Out.WriteLine($"Button {btnId} pressed");
                            _stick.SetBtn(true, _stickId, btnId);
                            break;
                        case "RELEASE":
                            Console.Out.WriteLine($"Button {btnId} released");
                            _stick.SetBtn(false, _stickId, btnId);
                            break;
                    }
                    break;
                case "AXIS":
                    break;
            }
            //return await Task.FromResult("OK");
            await SendText(Encoding.Default.GetBytes("OK"), true);
            //return base.OnMessageReceived(message, type);
        }

        private void InitStick()
        {
            if (_stick != null)
            {
                return;
            }

            uint id = 1;

            _stick = new vJoy();

            if (!_stick.vJoyEnabled())
            {
                Console.Error.WriteLine("vJoy driver not enabled");
                _stick = null;
                return;
            }

            var status = _stick.GetVJDStatus(id);

            if (status != VjdStat.VJD_STAT_FREE)
            {
                Console.Error.WriteLine($"Could not acquire vJoy device {id}. Device Status = {status.ToString()}");
                _stick = null;
                return;
            }

            _buttonCount = _stick.GetVJDButtonNumber(id);

            Console.Out.WriteLine($"vJoy device {id} has {_buttonCount} buttons");

            if (!_stick.AcquireVJD(id))
            {
                Console.Error.WriteLine($"Failed to acquire vjoy device number {id}");
                _stick = null;
                return;
            }

            Console.Out.WriteLine($"Successfully acquired vJoy device {id}");

            for (uint idx = 1; idx <= _buttonCount; idx++)
            {
                _stick.SetBtn(false, _stickId, idx);
            }
        }
    }
}
