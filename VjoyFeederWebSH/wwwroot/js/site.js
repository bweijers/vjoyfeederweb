﻿$(document).ready(function () {
    $('.button')
        .mousedown(function () {
            var id = $(this).data('id');
            pressButton(id);
        })
        .mouseup(function () {
            var id = $(this).data('id');
            releaseButton(id);
        });
    openSocket();
});