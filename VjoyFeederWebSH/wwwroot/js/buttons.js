﻿var socket;

function openSocket() {
    var host = document.baseURI.replace("http", "ws") + "/ws"; //"ws://localhost:9000/ws";

    try {
        socket = new WebSocket(host);

        socket.onopen = function (openEvent) {
            log("Socket opened");
        }

        socket.onmessage = function (messageEvent) {
            log("Message received");
        }

        socket.onerror = function (errorEvent) {
            log("Error received");
        }

        socket.onclose = function (closeEvent) {
            log("Socket closed - " + closeEvent.code);
        }
    }
    catch (exception) {
        log(exception);
    }
}

function log(message) {
    if (window.console) {
        console.log(message);
    }
}

function sendMessage(message) {
    if (!socket || socket.readyState != WebSocket.OPEN) {
        log("No socket available");
        return;
    }
    log("Sending message: " + message);
    socket.send(message);
}

function pressButton(buttonId) {
    sendMessage("BTN PRESS " + buttonId);
}

function releaseButton(buttonId) {
    sendMessage("BTN RELEASE " + buttonId);
}